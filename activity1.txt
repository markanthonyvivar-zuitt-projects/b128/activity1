 List the books Authored by Marjorie Green.
 1. The Busy Executive's Database Guide
 2. You can combat computer stress


 List the books Authored by Michael O'Leary.
 1. Cooking with computers
 

 Write the author/s of "The Busy Executives Database Guide".
 1. Marjorie Green
 2. Abraham Bennet



 Identify the publisher of "But Is It User Friendly?".
 1. Algodata Infosystems


 List the books published by Algodata Infosystems.
 1. The Busy Executive's Database Guide
 2. Cooking with computers
 3. Straight talk about computers
 4. But is it user friendly?
 5. Secrets of Silicon Valley
 6. Net Etiquette